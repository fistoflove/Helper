<?php

namespace Annex\Helper\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Image extends AbstractHelper
{

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
    }

    public function sayHello()
    {
        echo "hello from ImageHelper";
    }

    public function getStaffImage($name)
    {
        $url = false;
        if (is_string($name)) {
            $url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'StaffMemberPictures/' . $name . '.png';
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(__('Something went wrong while getting the image url.'));
            }
        return $url;
    }

    public function getImgUrl($name)
    {
        $url = false;
        if (is_string($name)) {
            $url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'pub/media/' . $name;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(__('Something went wrong while getting the image url.'));
            }
        return $url;
    }
}
