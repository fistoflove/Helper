<?php

namespace Annex\Helper\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Registry extends AbstractHelper
{

    protected $_registry;

    public function __construct(
        \Magento\Framework\Registry $registry
    )
    {
        $this->_registry = $registry;
    }

    public function sayHello()
    {
        echo "hello from Registry Helper";
    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

}