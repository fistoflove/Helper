<?php

namespace Annex\Helper\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Collection extends AbstractHelper
{

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory $bestSellersCollectionFactory
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->bestSellersCollectionFactory = $bestSellersCollectionFactory;
    }

    public function sayHello()
    {
        echo "hello from CollectionHelper";
    }

    public function sayYes()
    {
        echo "hello from yes";
    }

    public function getCollection($cat_id)
    {
        $productIds = [];

        $bestSellers = $this->bestSellersCollectionFactory->create()->setPeriod('month');

        foreach ($bestSellers as $product) {
            $productIds[] = $product->getProductId();
        }
        // echo json_encode($productIds);

        $collection = $this->productCollectionFactory->create();

        $collection->setStoreId(1);

        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection->addStoreFilter()
                    ->addAttributeToSort('entity_id', 'desc')
                    ->setPageSize(4)
                    ->addCategoriesFilter(['in' => $cat_id])
                    ->setCurPage(1);

        $collection->distinct(true);

        return $collection;
    }
}
