<?php

namespace Annex\Helper\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Url\Helper\Data as UrlHelper;

class Related extends AbstractHelper
{

    private $urlHelper;

    public function __construct(
        UrlHelper $urlHelper
    )
    {
        $this->urlHelper = $urlHelper;
    }

    public function sayHello()
    {
        echo "hello from Related Helper";
    }

    /**
     * Wrapper for the PostHelper::getPostData()
     *
     * @param string $url
     * @param array $data
     * @return array
     */
    public function getPostData(string $url, array $data = []):array
    {
        if (!isset($data[ActionInterface::PARAM_NAME_URL_ENCODED])) {
            $data[ActionInterface::PARAM_NAME_URL_ENCODED] = $this->urlHelper->getEncodedUrl();
        }
        return ['action' => $url, 'data' => $data];
    }

}