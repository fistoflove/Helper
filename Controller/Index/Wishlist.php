<?php

namespace Annex\Helper\Controller\Index;
use \Magento\Framework\App\Action\Action;

class Wishlist extends Action {

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
        ) {
            parent::__construct($context);
            $this->wishlistHelper = $wishlistHelper;
            $this->jsonFactory = $jsonFactory;
    }

    public function execute() {
        $result = [];
        $data = $this->wishlistHelper->getWishlistItemCollection()->getData();
        foreach($data as $k=>$v) {
            array_push($result, $v['product_id']);
        }
        

        return $this->jsonFactory->create()->setData(['status' => 200,'items' => $result]);
    }
}