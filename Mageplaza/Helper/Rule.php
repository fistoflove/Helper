<?php

namespace Annex\Helper\Mageplaza\Helper;

use Magento\Catalog\Model\ResourceModel\Product\Collection as CatalogCollection;
use Magento\Framework\DB\Select;
use Mageplaza\AutoRelated\Model\Rule as AutoRelatedRule;
use Mageplaza\AutoRelated\Model\Config\Source\Direction;
use Zend_Db_Expr;

class Rule extends \Mageplaza\AutoRelated\Helper\Rule
{
    public function sayHello()
    {
        echo "hello from Mageplaza's Block Class";
    }

    /**
     * @param AutoRelatedRule $rule
     * @param CatalogCollection $collection
     */
    public function sortProduct(AutoRelatedRule $rule, CatalogCollection $collection)
    {
        if ($rule->getLimitNumber()) {
            $limit = $rule->getLimitNumber();
        } else {
            $limit = 4;
        }
        switch ($rule->getSortOrderDirection()) {
            case Direction::BESTSELLER:
                $productIds = [];
                $collection->getSelect()->limit($limit)->joinLeft(
                    ['soi' => $collection->getTable('sales_bestsellers_aggregated_yearly')],
                    'e.entity_id = soi.product_id',
                    ['qty_ordered' => 'SUM(soi.qty_ordered)']
                )
                    ->group('e.entity_id')
                    ->order('qty_ordered DESC');
                foreach ($collection->getItems() as $product) {
                    if (in_array($product->getId(), $productIds, true)) {
                        continue;
                    }
                    $parentId = $this->getFirstParentId($product);
                    if ($parentId) {
                        $productIds[] = $parentId;
                    } elseif ($product->getData('visibility') != 1) {
                        $productIds[] = $product->getId();
                    }
                }

                $collection = $rule->getProductCollectionVisibility();
                $collection->getSelect()->where('e.entity_id IN (?)', $productIds);
                $collection->getSelect()->reset(Select::ORDER);
                $collection->getSelect()
                    ->order(new Zend_Db_Expr('FIELD(e.entity_id,' . implode(',', $productIds) . ')'));

                break;
            case Direction::PRICE_LOW:
                $collection->addAttributeToSort('price', 'ASC');
                break;
            case Direction::PRICE_HIGH:
                $collection->addAttributeToSort('price', 'DESC');
                break;
            case Direction::NEWEST:
                $collection->getSelect()->order('e.created_at DESC');
                break;
            default:
                $collection->getSelect()->order('rand()');
                break;
        }

        return $collection;
    }

    /**
     * @param $product
     *
     * @return string|null
     */
    private function getFirstParentId($product)
    {
        $configurableProducts = $this->configurableType->getParentIdsByChild($product->getId());
        if (!empty($configurableProducts)) {
            return array_shift($configurableProducts);
        }

        $groupedProducts = $this->groupedType->getParentIdsByChild($product->getId());
        if (!empty($groupedProducts)) {
            return array_shift($groupedProducts);
        }

        $bundleProducts = $this->bundleSelection->getParentIdsByChild($product->getId());
        if (!empty($bundleProducts)) {
            return array_shift($bundleProducts);
        }

        return null;
    }

}
