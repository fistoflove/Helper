<?php

namespace Annex\Helper\Block\Rewrite;

use Magento\Catalog\Block\Product\ListProduct as MagentoListProduct;

class ProductHelper extends MagentoListProduct
{

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Review\Model\Rating $ratingModel,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Variable\Model\Variable $customVar,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet,
        \Magento\Review\Model\ResourceModel\Rating\Option\Vote\CollectionFactory $voteCollection,
        array $data = []
    ) {
        $this->_catalogLayer = $layerResolver->get();
        $this->_postDataHelper = $postDataHelper;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->urlHelper = $urlHelper;
        $this->customerSession = $customerSession;
        $this->localeDate = $localeDate;
        $this->customVar = $customVar;
        $this->productModel = $productModel;
        $this->ratingModel = $ratingModel;
        $this->reviewFactory = $reviewFactory;
        $this->voteCollection = $voteCollection;
        $this->attributeSet = $attributeSet;
        parent::__construct($context, $postDataHelper , $layerResolver, $categoryRepository, $urlHelper, $data);
    }

    public function sayHello()
    {
        echo "hello from ProductHelper";
    }

    public function lubeBlockId()
    {
        return $this->customVar->loadByCode('lube_product_block_id')->getPlainValue();
    }

    public function freeShippingValue()
    {
        return $this->customVar->loadByCode('free_shippings_value_threshold')->getPlainValue();
    }

    public function getTheReviews($_product)
    {
        $collection = $this->reviewFactory->create()->getResourceCollection()->addStoreFilter(1)->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)->addEntityFilter('product',$_product->getId())->setDateOrder();
        $data = $collection->getData();
        $result = [];
        foreach($data as $key=>$review) {
            $rating = $this->voteCollection->create()->addRatingInfo()->addOptionInfo()->addRatingOptions()->addFieldToFilter('review_id',$review['review_id']);
            $review_ratings = [];

            if(count($rating->getData()) > 0 ) {
                foreach($rating->getData() as $v) {array_push($review_ratings, number_format(($v['percent'] / 100) * 5, 1));}

                $review['rating'] = number_format(array_sum($review_ratings) / count($review_ratings), 2);
            } else {
                $review['rating'] = 0.00;
            }
            array_push($result, $review);
        }
        return $result;
    }

    public function getTheAttributeSet($product)
    {
        $attributeSetRepository = $this->attributeSet->get($product->getAttributeSetId());
        return $attributeSetRepository->getAttributeSetName();
    }

    public function getTheAttributes($product)
    {
        $type = $this->getTheAttributeSet($product);
        $all_attributes = [
            'Wine' => ['body', 'origin', 'region', 'colour_style', 'grape'],
            'Spirit' => ['origin', 'colour'],
            'Beer' => ['origin', 'colour']
        ];
        $result = [];
        $i = 1;
        if(array_key_exists($type, $all_attributes)) {
            foreach($all_attributes[$type] as $k=>$v) {
                $attr = $product->getResource()->getAttribute($v);
                if($attr) {
                    $attr_v = $attr->getFrontend()->getValue($product);
                    $attr_l = $attr->getFrontend()->getLabel($product);
                    if($attr_v) {
                        array_push($result, [
                            'value' => $attr_v, 
                            'label' => $attr_l,
                            'order' => numfmt_format(new \NumberFormatter('en', \NumberFormatter::SPELLOUT), $i++)
                        ]);
                    }
                }
            }
            if(count($result) > 0 ) {
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getTheData($_product)
    {
        $superAttributeList = [];
        $RatingOb = $this->ratingModel->getEntitySummary($_product->getId());
        $ratings = $this->reviewFactory->create()->getResourceCollection()->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)->addEntityFilter('product', $_product->getId());
        $review_count = count($ratings);
        $r_sum = $RatingOb->getSum();
        $r_count = $RatingOb->getCount();
        $newsFromDate = $_product->getNewsFromDate();
        $newsToDate = $_product->getNewsToDate();
        $data = [
            "name"  => $_product->getName(),
            "price"     =>  $_product->getPrice(),
            "sprice"    => $_product->getSpecialPrice(),
            "fprice"    => $_product->getFinalPrice(),
            "url"       => $_product->getProductUrl(),
            "rcount"    =>  $review_count,
            "r_sum"     => $r_sum,
            "r_count"   => $r_count,
            "review_count"     => $review_count,
            "new"       => (!$newsFromDate && !$newsToDate ? false : $this->localeDate->isScopeDateInInterval($_product->getStore(),$newsFromDate,$newsToDate)),
            "fship"     => ($_product->getFinalPrice() > $this->freeShippingValue() ? true : false)
        ];
        if ($data["price"] > $data["fprice"] ) {
            $data["pchange"] = round((1 - $data["fprice"] / $data["price"] ) * 100);
            $data["sale"]  =   true;
        } else {
            $data["pchange"] = 0;
            $data["sale"]  =  false;
        }

        if ($review_count == 0 || $r_sum == 0 || $r_count == 0) {
            $data["rating"] = 0;
        } else {
            $rating = $r_sum / $r_count;
            $data["rating"] = number_format(($rating / 100) * 5, 1);
        }

        if ($_product->getTypeId() != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            $data["options"] = 0;
        } else {
            $productTypeInstance = $_product->getTypeInstance();
            $productTypeInstance->setStoreFilter($_product->getStoreId(), $_product);
            $attributes = $productTypeInstance->getConfigurableAttributes($_product);
            $_children = $_product->getTypeInstance()->getUsedProducts($_product);
            $superAttributeList = ["count" => count($_children),];
            foreach ($attributes as $_attribute) {
                $attributeCode = $_attribute->getProductAttribute()->getAttributeCode();
                $superAttributeList["name"] = $attributeCode;
            }
            $data["options"] = $superAttributeList["count"];
        }
        return $data;
    }
}
